-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for soga_sys
CREATE DATABASE IF NOT EXISTS `soga_sys` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `soga_sys`;

-- Dumping structure for table soga_sys.auth_access
CREATE TABLE IF NOT EXISTS `auth_access` (
  `id` int(10) unsigned NOT NULL COMMENT 'id',
  `route_link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Đường dẫn route',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Xác định user nào, thuộc group auth nào, role nào được phép truy cập một đường dẫn';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_groups
CREATE TABLE IF NOT EXISTS `auth_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth_role_id` int(10) unsigned DEFAULT NULL COMMENT 'mã phân quyền',
  `auth_group_id` int(10) unsigned DEFAULT NULL COMMENT 'ghi theo dòng các group được truy cập trong role, vì group có thể rất nhiều nên phải lưu khác với module',
  `auth_route_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mục được truy cập trong danh sách controller, nếu trống tức là được truy cập tất cả ',
  `action_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'funciton thuộc route được phép truy cập',
  `obj_value` int(10) unsigned DEFAULT NULL COMMENT 'mã của dòng được phép tác động, nếu rỗng thì được phép tác động tất cả',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_module
CREATE TABLE IF NOT EXISTS `auth_module` (
  `id` int(10) unsigned NOT NULL COMMENT 'id',
  `auth_role_id` int(10) unsigned NOT NULL COMMENT 'mã phân quyền',
  `auth_modules` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'danh sách các mục thuộc các module được phép, cách nhau bởi dấu phẩy và không có khoảng trắng',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_role_id` (`auth_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT=' Xác định phân quyền nào được kết nối module nào';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_permissions
CREATE TABLE IF NOT EXISTS `auth_permissions` (
  `permission_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `route_id` int(10) unsigned NOT NULL COMMENT 'id route action được phép thực hiện',
  `role_id` int(10) unsigned NOT NULL COMMENT 'phân quyền được phép thực hiện',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Các quyền được phân công';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_roles
CREATE TABLE IF NOT EXISTS `auth_roles` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên phân quyền',
  `role_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `role_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'chú thích về phân quyền',
  `role_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái phân quyền',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`),
  UNIQUE KEY `role_slug` (`role_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Thông tin phân quyền';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_role_users
CREATE TABLE IF NOT EXISTS `auth_role_users` (
  `role_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id tài khoản',
  `role_id` int(10) unsigned NOT NULL COMMENT 'id phân quyền',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`role_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Liên kết giữa phân quyền và tài khoảnQuan hệ 1 nhiều';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_routes
CREATE TABLE IF NOT EXISTS `auth_routes` (
  `route_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `route_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'route của đối tượng action url',
  `route_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'kiểu của route (get, post, put, delete)',
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đặt lại của action url',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`route_id`),
  UNIQUE KEY `route_name` (`route_name`),
  UNIQUE KEY `route_link` (`route_link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Tên các action trong danh sách xử lý';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.config_menu
CREATE TABLE IF NOT EXISTS `config_menu` (
  `cf_menu_id` int(10) NOT NULL,
  `cf_menu_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cf_menu_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.config_page
CREATE TABLE IF NOT EXISTS `config_page` (
  `cf_page_id` int(10) NOT NULL,
  `cf_page_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cf_page_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.excel_configs
CREATE TABLE IF NOT EXISTS `excel_configs` (
  `excel_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `excel_config_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên của trường thông tin',
  `excel_config_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'kiểu của thông tin (int, string, date)',
  `excel_config_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị của trường thông tin',
  `excel_config_position` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'vị trí đặt',
  `excel_config_include_cells` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'gộp ô chiều ngang và dọc (định dạng ngang, dọc)',
  `excel_config_version` tinyint(2) unsigned NOT NULL COMMENT 'phiên bản sử dụng',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`excel_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu hình xuất excel';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.excel_templates
CREATE TABLE IF NOT EXISTS `excel_templates` (
  `excel_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `excel_template_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên template',
  `excel_template_note` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'chú thích',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`excel_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu trúc mẫu excel';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên group',
  `group_icon` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'icon của group',
  `group_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `group_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'loại group (music, video, image, chat)',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nhóm một danh sách các tài khoản với nhau';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.group_users
CREATE TABLE IF NOT EXISTS `group_users` (
  `group_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) DEFAULT NULL COMMENT 'id tài khoản',
  `group_id` int(10) DEFAULT NULL COMMENT 'id group',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`group_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ các user trong một group';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.mail_logs
CREATE TABLE IF NOT EXISTS `mail_logs` (
  `mail_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mail_template_id` int(10) unsigned NOT NULL COMMENT 'id mẫu email',
  `mail_log_send_datetime` datetime NOT NULL COMMENT 'thời gian gửi',
  `mail_log_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'địa chỉ nhận',
  `mail_log_subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề',
  `mail_log_body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mail_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ các mail sẽ gửi';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.mail_templates
CREATE TABLE IF NOT EXISTS `mail_templates` (
  `mail_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id người tạo',
  `subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề',
  `body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mail_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Mẫu email tạo sẵn';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_categories
CREATE TABLE IF NOT EXISTS `master_categories` (
  `mt_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mt_parent_id` int(10) NOT NULL COMMENT 'id mục cha',
  `mt_category_icon` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'icon của mục',
  `mt_category_thumbnail` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ảnh bìa chuyên mục',
  `mt_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên chuyên đề',
  `mt_category_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái danh mục',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mt_category_id`),
  UNIQUE KEY `category_name_category_slug` (`mt_category_name`,`mt_category_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Chuyên đề các bài đăng';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_categorys_associates
CREATE TABLE IF NOT EXISTS `master_categorys_associates` (
  `mt_categorys_associate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mt_category_id` int(10) unsigned NOT NULL COMMENT 'id chuyên mục',
  `mt_object_id` int(10) unsigned NOT NULL COMMENT 'id bài post, ảnh, bài hát hoặc video',
  `mt_categorys_associate_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'loại category (post, image, music, video)',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mt_categorys_associate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Liên kết chuyên mục với bài viết, bài hát, ảnh hay videoQuan hệ nhiều nhiều (một bài viết có thể có nhiều chuyên mục khác nhau, ngược lại 1 chuyên mục có thể có nhiều bài viết)';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_cities
CREATE TABLE IF NOT EXISTS `master_cities` (
  `mt_city_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `mt_city_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên thành phố',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mt_city_id`),
  UNIQUE KEY `city_name` (`mt_city_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Tỉnh thành';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_districts
CREATE TABLE IF NOT EXISTS `master_districts` (
  `district_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(10) unsigned NOT NULL COMMENT 'id thành phố',
  `district_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên quận huyện',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Quận huyện';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_wards
CREATE TABLE IF NOT EXISTS `master_wards` (
  `ward_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `district_id` int(10) NOT NULL COMMENT 'id quận huyện',
  `ward_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên phường xã',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Phường xã';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.media_audios
CREATE TABLE IF NOT EXISTS `media_audios` (
  `music_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'người tải lên',
  `music_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `music_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa file',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`music_id`),
  UNIQUE KEY `music_slug` (`music_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Lưu trữ thông tin bài hát';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.media_images
CREATE TABLE IF NOT EXISTS `media_images` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `image_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `image_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa ảnh',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `video_slug` (`image_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Lưu trữ ảnh';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.media_videos
CREATE TABLE IF NOT EXISTS `media_videos` (
  `video_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `video_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `video_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa video',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`video_id`),
  UNIQUE KEY `video_slug` (`video_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Lưu trữ thông tin video giải trí';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.options
CREATE TABLE IF NOT EXISTS `options` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `option_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên mục cấu hình',
  `option_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu hình hệ thống';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_comments
CREATE TABLE IF NOT EXISTS `post_comments` (
  `po_comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `po_object_id` int(10) unsigned NOT NULL COMMENT 'id đối tượng được comment',
  `au_user_id` int(10) unsigned NOT NULL COMMENT 'id user đã bình luận',
  `po_comment_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung bình luận',
  `po_comment_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'bình luận cho đối tượng nào (post, video, music, image)',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái comment',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`po_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Bình luận cho 1 bài post';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_posts
CREATE TABLE IF NOT EXISTS `post_posts` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `post_title` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề bài viết',
  `post_thumbnail` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ảnh bìa bài viết',
  `post_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung bài viết',
  `post_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `post_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái bài viết, duyệt , chưa duyệt ,...',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`post_id`),
  UNIQUE KEY `post_slug` (`post_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Thông tin bài đăng';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_tags
CREATE TABLE IF NOT EXISTS `post_tags` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên chuyên đề',
  `tag_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Lưu trữ các tag cho các bài viết';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_tag_associates
CREATE TABLE IF NOT EXISTS `post_tag_associates` (
  `tag_associate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'id chuyên mục',
  `object_id` int(10) unsigned NOT NULL COMMENT 'id đối tượng liên kết',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`tag_associate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Lưu trữ quan hệ giữa tag vào bài post';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_views
CREATE TABLE IF NOT EXISTS `post_views` (
  `post_view_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `post_id` int(10) unsigned NOT NULL COMMENT 'id bai viet',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT 'id user',
  `post_is_like` tinyint(1) DEFAULT 0 COMMENT 'trạng thái like (0: chưa like, 1: đã like)',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`post_view_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu thông tin về việc người khác đã xem bài post\r\nNếu 1 user xem bài post thì sẽ tự sinh thêm một dòng';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `profile_template_id` int(10) unsigned NOT NULL COMMENT 'id cấu trúc',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id tài khoản',
  `profile_values` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Thông tin chi tiết tài khoản';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.profile_template
CREATE TABLE IF NOT EXISTS `profile_template` (
  `profile_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(10) unsigned NOT NULL COMMENT 'phân quyền có trường thông tin này',
  `profile_template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên của trường thông tin',
  `profile_template_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'kiểu của thông tin (int, string, date)',
  `profile_template_default` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị mặc định của trường thông tin',
  `profile_template_version` tinyint(2) unsigned NOT NULL COMMENT 'phiên bản sử dụng',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`profile_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cấu trúc lưu trữ thông tin cho mỗi phân quyền';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đệm và tên',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'họ',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đăng nhập',
  `password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'mật khẩu đăng nhập',
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sđt',
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email',
  `device_token` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'token thiết bị',
  `active_code` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'key kích hoạt tài khoản',
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'trạng thái user',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ thông tin người dùng';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
