-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.18 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for sgt_music
CREATE DATABASE IF NOT EXISTS `sgt_music` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `sgt_music`;

-- Dumping structure for table sgt_music.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` int(10) NOT NULL COMMENT 'id mục cha',
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên chuyên đề',
  `category_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `category_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái danh mục',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_name_category_slug` (`category_name`,`category_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Chuyên đề các bài đăng';

-- Dumping data for table sgt_music.categories: ~0 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table sgt_music.categorys_associates
CREATE TABLE IF NOT EXISTS `categorys_associates` (
  `categorys_associate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category_id` int(10) unsigned NOT NULL COMMENT 'id chuyên mục',
  `object_id` int(10) unsigned NOT NULL COMMENT 'id bài post, ảnh, bài hát hoặc video',
  `categorys_associate_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'loại category (post, image, music, video)',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`categorys_associate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Liên kết chuyên mục với bài viết, bài hát, ảnh hay video\r\nQuan hệ nhiều nhiều (một bài viết có thể có nhiều chuyên mục khác nhau, ngược lại 1 chuyên mục có thể có nhiều bài viết)';

-- Dumping data for table sgt_music.categorys_associates: ~0 rows (approximately)
DELETE FROM `categorys_associates`;
/*!40000 ALTER TABLE `categorys_associates` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorys_associates` ENABLE KEYS */;

-- Dumping structure for table sgt_music.cities
CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `city_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên thành phố',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_name` (`city_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tỉnh thành';

-- Dumping data for table sgt_music.cities: ~0 rows (approximately)
DELETE FROM `cities`;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Dumping structure for table sgt_music.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'id đối tượng được comment',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung bình luận',
  `comment_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'bình luận cho đối tượng nào (post, video, music, image)',
  `comment_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái comment',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id user đã bình luận',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bình luận cho 1 bài post';

-- Dumping data for table sgt_music.comments: ~0 rows (approximately)
DELETE FROM `comments`;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table sgt_music.districts
CREATE TABLE IF NOT EXISTS `districts` (
  `district_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(10) unsigned NOT NULL DEFAULT '0',
  `district_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Quận huyện';

-- Dumping data for table sgt_music.districts: ~0 rows (approximately)
DELETE FROM `districts`;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;

-- Dumping structure for table sgt_music.excel_configs
CREATE TABLE IF NOT EXISTS `excel_configs` (
  `excel_config_id` int(10) NOT NULL COMMENT 'id',
  `excel_config_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên của trường thông tin',
  `excel_config_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'kiểu của thông tin (int, string, date)',
  `excel_config_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị của trường thông tin',
  `excel_config_position` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'vị trí đặt',
  `excel_config_include_cells` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'gộp ô chiều ngang và dọc (định dạng ngang, dọc)',
  `excel_config_version` tinyint(2) unsigned NOT NULL COMMENT 'phiên bản sử dụng',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`excel_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu hình xuất excel';

-- Dumping data for table sgt_music.excel_configs: ~0 rows (approximately)
DELETE FROM `excel_configs`;
/*!40000 ALTER TABLE `excel_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `excel_configs` ENABLE KEYS */;

-- Dumping structure for table sgt_music.excel_templates
CREATE TABLE IF NOT EXISTS `excel_templates` (
  `excel_template_id` int(10) DEFAULT NULL COMMENT 'id',
  `excel_template_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên template',
  `excel_template_note` text COLLATE utf8_unicode_ci COMMENT 'chú thích',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu trúc mẫu excel';

-- Dumping data for table sgt_music.excel_templates: ~0 rows (approximately)
DELETE FROM `excel_templates`;
/*!40000 ALTER TABLE `excel_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `excel_templates` ENABLE KEYS */;

-- Dumping structure for table sgt_music.images
CREATE TABLE IF NOT EXISTS `images` (
  `image_id` int(10) unsigned NOT NULL COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `video_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `image_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa video',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `video_slug` (`video_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ ảnh';

-- Dumping data for table sgt_music.images: ~0 rows (approximately)
DELETE FROM `images`;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Dumping structure for table sgt_music.mail_logs
CREATE TABLE IF NOT EXISTS `mail_logs` (
  `mail_log_id` int(11) unsigned NOT NULL COMMENT 'id',
  `mail_template_id` int(11) NOT NULL COMMENT 'id mẫu email',
  `mail_log_send_datetime` datetime NOT NULL COMMENT 'thời gian gửi',
  `mail_log_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'địa chỉ nhận',
  `mail_log_subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề',
  `mail_log_body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mail_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ các mail sẽ gửi';

-- Dumping data for table sgt_music.mail_logs: ~0 rows (approximately)
DELETE FROM `mail_logs`;
/*!40000 ALTER TABLE `mail_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_logs` ENABLE KEYS */;

-- Dumping structure for table sgt_music.mail_templates
CREATE TABLE IF NOT EXISTS `mail_templates` (
  `mail_template_id` int(11) unsigned NOT NULL COMMENT 'id',
  `user_id` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'id người tạo',
  `subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề',
  `body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`mail_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Mẫu email tạo sẵn';

-- Dumping data for table sgt_music.mail_templates: ~0 rows (approximately)
DELETE FROM `mail_templates`;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;

-- Dumping structure for table sgt_music.musics
CREATE TABLE IF NOT EXISTS `musics` (
  `music_id` int(10) unsigned NOT NULL COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `music_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `music_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa video',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`music_id`),
  UNIQUE KEY `music_slug` (`music_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ thông tin bài hát';

-- Dumping data for table sgt_music.musics: ~0 rows (approximately)
DELETE FROM `musics`;
/*!40000 ALTER TABLE `musics` DISABLE KEYS */;
/*!40000 ALTER TABLE `musics` ENABLE KEYS */;

-- Dumping structure for table sgt_music.options
CREATE TABLE IF NOT EXISTS `options` (
  `option_id` int(10) NOT NULL COMMENT 'id',
  `option_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên mục cấu hình',
  `option_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu hình hệ thống';

-- Dumping data for table sgt_music.options: ~0 rows (approximately)
DELETE FROM `options`;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
/*!40000 ALTER TABLE `options` ENABLE KEYS */;

-- Dumping structure for table sgt_music.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `route_id` int(10) unsigned NOT NULL COMMENT 'id route action được phép thực hiện',
  `role_id` int(10) unsigned NOT NULL COMMENT 'phân quyền được phép thực hiện',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Các quyền được phân công';

-- Dumping data for table sgt_music.permissions: ~0 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table sgt_music.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `post_title` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề bài viết',
  `post_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung bài viết',
  `post_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `post_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái bài viết, duyệt , chưa duyệt ,...',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`post_id`),
  UNIQUE KEY `post_slug` (`post_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Thông tin bài đăng';

-- Dumping data for table sgt_music.posts: ~0 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table sgt_music.post_views
CREATE TABLE IF NOT EXISTS `post_views` (
  `post_view_id` int(10) NOT NULL COMMENT 'id',
  `post_id` int(10) NOT NULL COMMENT 'id bai viet',
  `user_id` int(10) DEFAULT NULL COMMENT 'id user',
  `post_is_like` tinyint(1) DEFAULT '0' COMMENT 'trạng thái like (0: chưa like, 1: đã like)',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`post_view_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu thông tin về việc người khác đã xem bài post\r\nNếu 1 user xem bài post thì sẽ tự sinh thêm một dòng';

-- Dumping data for table sgt_music.post_views: ~0 rows (approximately)
DELETE FROM `post_views`;
/*!40000 ALTER TABLE `post_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_views` ENABLE KEYS */;

-- Dumping structure for table sgt_music.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `profile_structure_id` int(10) unsigned NOT NULL COMMENT 'id cấu trúc',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id tài khoản',
  `profile_values` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Thông tin chi tiết tài khoản';

-- Dumping data for table sgt_music.profiles: ~0 rows (approximately)
DELETE FROM `profiles`;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;

-- Dumping structure for table sgt_music.profile_template
CREATE TABLE IF NOT EXISTS `profile_template` (
  `profile_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(10) unsigned NOT NULL COMMENT 'phân quyền có trường thông tin này',
  `profile_template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên của trường thông tin',
  `profile_template_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'kiểu của thông tin (int, string, date)',
  `profile_template_default` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị mặc định của trường thông tin',
  `profile_template_version` tinyint(2) unsigned NOT NULL COMMENT 'phiên bản sử dụng',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`profile_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cấu trúc lưu trữ thông tin cho mỗi phân quyền';

-- Dumping data for table sgt_music.profile_template: ~0 rows (approximately)
DELETE FROM `profile_template`;
/*!40000 ALTER TABLE `profile_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_template` ENABLE KEYS */;

-- Dumping structure for table sgt_music.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên phân quyền',
  `role_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'chú thích về phân quyền',
  `role_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái phân quyền',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`),
  UNIQUE KEY `role_slug` (`role_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Thông tin phân quyền';

-- Dumping data for table sgt_music.roles: ~0 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table sgt_music.role_users
CREATE TABLE IF NOT EXISTS `role_users` (
  `role_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id tài khoản',
  `role_id` int(10) unsigned NOT NULL COMMENT 'id phân quyền',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`role_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Liên kết giữa phân quyền và tài khoản\r\nQuan hệ 1 nhiều';

-- Dumping data for table sgt_music.role_users: ~0 rows (approximately)
DELETE FROM `role_users`;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;

-- Dumping structure for table sgt_music.routes
CREATE TABLE IF NOT EXISTS `routes` (
  `route_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`route_id`),
  UNIQUE KEY `route_name` (`route_name`),
  UNIQUE KEY `route_link` (`route_link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tên các action trong danh sách xử lý';

-- Dumping data for table sgt_music.routes: ~0 rows (approximately)
DELETE FROM `routes`;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;

-- Dumping structure for table sgt_music.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(10) unsigned NOT NULL COMMENT 'id',
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên chuyên đề',
  `tag_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ các tag cho các bài viết';

-- Dumping data for table sgt_music.tags: ~0 rows (approximately)
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table sgt_music.tag_associates
CREATE TABLE IF NOT EXISTS `tag_associates` (
  `tag_associate_id` int(10) unsigned NOT NULL COMMENT 'id',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'id chuyên mục',
  `object_id` int(10) unsigned NOT NULL COMMENT 'id đối tượng liên kết',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`tag_associate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ quan hệ giữa tag vào bài post';

-- Dumping data for table sgt_music.tag_associates: ~0 rows (approximately)
DELETE FROM `tag_associates`;
/*!40000 ALTER TABLE `tag_associates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_associates` ENABLE KEYS */;

-- Dumping structure for table sgt_music.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đệm và tên',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'họ',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đăng nhập',
  `password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'mật khẩu đăng nhập',
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sđt',
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email',
  `device_token` text COLLATE utf8_unicode_ci COMMENT 'token thiết bị',
  `active_code` text COLLATE utf8_unicode_ci COMMENT 'key kích hoạt tài khoản',
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'trạng thái user',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ thông tin người dùng';

-- Dumping data for table sgt_music.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table sgt_music.videos
CREATE TABLE IF NOT EXISTS `videos` (
  `video_id` int(10) unsigned NOT NULL COMMENT 'id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'tác giả bài viết',
  `video_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `video_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa video',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`video_id`),
  UNIQUE KEY `video_slug` (`video_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ thông tin video giải trí';

-- Dumping data for table sgt_music.videos: ~0 rows (approximately)
DELETE FROM `videos`;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;

-- Dumping structure for table sgt_music.wards
CREATE TABLE IF NOT EXISTS `wards` (
  `ward_id` int(10) unsigned NOT NULL,
  `district_id` int(10) NOT NULL,
  `ward_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngày cập nhật',
  PRIMARY KEY (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Phường xã';

-- Dumping data for table sgt_music.wards: ~0 rows (approximately)
DELETE FROM `wards`;
/*!40000 ALTER TABLE `wards` DISABLE KEYS */;
/*!40000 ALTER TABLE `wards` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
