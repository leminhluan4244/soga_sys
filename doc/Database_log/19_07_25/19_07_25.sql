-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for soga_sys
CREATE DATABASE IF NOT EXISTS `soga_sys` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `soga_sys`;

-- Dumping structure for table soga_sys.auth_access
CREATE TABLE IF NOT EXISTS `auth_access` (
  `id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Giới hạn phân quyền được truy cập',
  `route_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Khóa này liên kết tới route_link chức năng',
  `access_only_creater` smallint(1) unsigned NOT NULL COMMENT 'Chỉ cho phép người tạo được phép truy cập (khi giá trị  bằng 1)',
  `access_only_group` smallint(1) unsigned NOT NULL COMMENT 'Chỉ cho phép các group được chia sẻ',
  `access_time_lock` datetime NOT NULL COMMENT 'Nếu trường này có giá trị thì tức là chức năng sẽ bị khóa đến thời điểm hiện tại',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Xác định user nào, thuộc group auth nào, role nào được phép truy cập một đường dẫn';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_groups
CREATE TABLE IF NOT EXISTS `auth_groups` (
  `id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_icon` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Icon của group',
  `group_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên ngắn tắt',
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên group',
  `group_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại group (music, video, image, chat, role)',
  `creater` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Người tạo ra group cũng là trưởng nhóm group đó',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_deleted` smallint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_slug` (`group_slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Thông tin group';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_group_users
CREATE TABLE IF NOT EXISTS `auth_group_users` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID thành viên group',
  `group_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID group',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ các user trong một group';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_roles
CREATE TABLE IF NOT EXISTS `auth_roles` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `role_slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên ngắn tắt cho phân quyền',
  `role_icon` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Icon cho phân quyền, trong đó phần tiền tố sẽ giúp xác định loại (class name, url, svg, ...)',
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên phân quyền',
  `role_note` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Chú thích',
  `role_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Trạng thái phân quyền',
  `creater` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Người tạo ra phân quyền',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  `is_deleted` smallint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`role_name`),
  UNIQUE KEY `role_slug` (`role_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Thông tin phân quyền';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_role_users
CREATE TABLE IF NOT EXISTS `auth_role_users` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `user_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id tài khoản',
  `role_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id phân quyền',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Liên kết giữa phân quyền và tài khoảnQuan hệ 1 nhiều';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.auth_routes
CREATE TABLE IF NOT EXISTS `auth_routes` (
  `id` int(10) unsigned NOT NULL,
  `route_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Route của đối tượng action url',
  `route_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Kiểu của route (get, post, put, delete), quy định biến này trong const',
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên đặt lại của action url',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `route_link` (`route_link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Tên các action trong danh sách xử lý';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.config_menu
CREATE TABLE IF NOT EXISTS `config_menu` (
  `id` int(10) NOT NULL,
  `menu_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên ngắn gọi menu',
  `menu_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên menu',
  `menu_content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung menu được lưu thành chuỗi json',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.config_page
CREATE TABLE IF NOT EXISTS `config_page` (
  `cf_page_id` int(10) NOT NULL,
  `cf_page_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cf_page_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.excel_configs
CREATE TABLE IF NOT EXISTS `excel_configs` (
  `excel_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `excel_config_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên của trường thông tin',
  `excel_config_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'kiểu của thông tin (int, string, date)',
  `excel_config_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị của trường thông tin',
  `excel_config_position` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'vị trí đặt',
  `excel_config_include_cells` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'gộp ô chiều ngang và dọc (định dạng ngang, dọc)',
  `excel_config_version` tinyint(2) unsigned NOT NULL COMMENT 'phiên bản sử dụng',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`excel_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu hình xuất excel';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.excel_templates
CREATE TABLE IF NOT EXISTS `excel_templates` (
  `excel_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `excel_template_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên template',
  `excel_template_note` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'chú thích',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`excel_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu trúc mẫu excel';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.mail_logs
CREATE TABLE IF NOT EXISTS `mail_logs` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `mail_template_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id mẫu email',
  `mail_log_send_datetime` datetime NOT NULL COMMENT 'thời gian gửi',
  `mail_log_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'địa chỉ nhận',
  `mail_log_subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề',
  `mail_log_body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ các mail sẽ gửi';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.mail_templates
CREATE TABLE IF NOT EXISTS `mail_templates` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'tiêu đề',
  `body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung',
  `creater` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id người tạo',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  `is_deleted` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Mẫu email tạo sẵn';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_categories
CREATE TABLE IF NOT EXISTS `master_categories` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) NOT NULL COMMENT 'ID mục cha',
  `category_icon` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Icon mục, sử dụng khi lên menu',
  `category_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên ngắn tắt',
  `category_thumbnail` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ảnh bìa',
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên chuyên đề',
  `category_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Loại chuyên mục (category post, media, tag)',
  `category_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'trạng thái danh mục',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  `is_deleted` smallint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_name_category_slug` (`category_name`,`category_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Chuyên đề';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_categorys_associates
CREATE TABLE IF NOT EXISTS `master_categorys_associates` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL COMMENT 'ID chuyên mục',
  `object_id` int(10) unsigned NOT NULL COMMENT 'ID bài post, ảnh, bài hát hoặc video',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Liên kết chuyên mục với bài viết, bài hát, ảnh hay videoQuan hệ nhiều nhiều (một bài viết có thể có nhiều chuyên mục khác nhau, ngược lại 1 chuyên mục có thể có nhiều bài viết)';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_cities
CREATE TABLE IF NOT EXISTS `master_cities` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `city_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên thành phố',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `city_name` (`city_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Tỉnh thành';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_districts
CREATE TABLE IF NOT EXISTS `master_districts` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(10) unsigned NOT NULL COMMENT 'ID tỉnh thành',
  `district_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên quận huyện',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `district_name` (`district_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Quận huyện';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.master_wards
CREATE TABLE IF NOT EXISTS `master_wards` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `district_id` int(10) NOT NULL COMMENT 'ID quận huyện',
  `ward_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên phường xã',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Phường xã';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.media_audios
CREATE TABLE IF NOT EXISTS `media_audios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `media_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên ngắn tắt cho route',
  `media_link` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'link chứa file',
  `media_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'loại media (music, image, video)',
  `creater` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_group` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `music_slug` (`media_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Lưu trữ thông tin bài hát';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_comments
CREATE TABLE IF NOT EXISTS `post_comments` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID bài đăng',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nội dụng bình luận',
  `comment_index` int(3) NOT NULL COMMENT 'Thứ tự comment trong danh sách bình luận',
  `comment_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Trạng thái bình luận',
  `creater` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID user bình luận',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Bình luận cho 1 bài post';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_posts
CREATE TABLE IF NOT EXISTS `post_posts` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `post_slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên ngắn tắt',
  `post_title` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tiêu đề',
  `post_thumbnail` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ảnh bìa',
  `template_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Template',
  `post_content` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nội dung',
  `post_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Trạng thái bài viết',
  `creater` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tác giả',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_slug` (`post_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Thông tin bài đăng';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.post_views
CREATE TABLE IF NOT EXISTS `post_views` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID bài viết',
  `user_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID người xem',
  `post_is_like` smallint(1) DEFAULT NULL COMMENT 'Có like ảnh hay không',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu thông tin về việc người khác đã xem bài post\r\nNếu 1 user xem bài post thì sẽ tự sinh thêm một dòng';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `template_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID cấu trúc',
  `user_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID tài khoản',
  `profile_values` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Giá trị',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Thông tin chi tiết tài khoản';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.profile_template
CREATE TABLE IF NOT EXISTS `profile_template` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Phân quyền nào sẽ sở hữu trường thông tin này',
  `profile_template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên trường thông tin',
  `profile_template_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Kiểu của thông tin (int, string, date)',
  `profile_template_default` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Giá trị mặc định của trường thông tin',
  `profile_template_version` tinyint(2) unsigned NOT NULL COMMENT 'Phiên bản',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cấu trúc lưu trữ thông tin cho mỗi phân quyền';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.setting_options
CREATE TABLE IF NOT EXISTS `setting_options` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên mục cấu hình',
  `setting_value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'giá trị',
  `setting_version` text COLLATE utf8_unicode_ci NOT NULL,
  `is_deleted` smallint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ cấu hình hệ thống';

-- Data exporting was unselected.
-- Dumping structure for table soga_sys.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đệm và tên',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'họ',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên đăng nhập',
  `password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'mật khẩu đăng nhập',
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sđt',
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email',
  `device_token` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'token thiết bị',
  `active_code` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'key kích hoạt tài khoản',
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'trạng thái user',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngày tạo',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ngày cập nhật',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lưu trữ thông tin người dùng';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
