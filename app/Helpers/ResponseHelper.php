<?php
/**
 * Created by PhpStorm.
 * User: luan.le
 * Date: 19/04/2019
 * Time: 11:51 PM
 */

namespace App\Helpers;

class ResponseHelper
{
    public static $name = "RequestHelper";

    public static function new()
    {
        return new static();
    }

    /**
     * Check and return response
     * @param  array $data
     * @param  string $name
     * @param  bool $data_null
     * @return object
     */
    public static function success($data = [], $name ="datas", $data_null = false)
    {
        if ($data_null) { // Only return code
            return response()
                ->json([
                    "code" => 200
                ]);
        } elseif (empty($data)) { // Return if data empty
            return response()
                ->json([
                    "code" => 200,
                    $name => []
                ]);
        } else { // Return data
            return response()
                ->json([
                    "code" => 200,
                    $name => $data
                ]);
        }
    }

    /**
     * Check and return exception
     * @param  object $errors
     * @param  int $code
     * @return object
     */
    public static function exception($exception, $code = 400)
    {
        return response()
            ->json([
                "code" => 500,
                "exceptions" => $exception
            ]);
    }

    /**
     * Return errors error
     * @param  object $errors
     * @param  int $code
     * @return object
     */
    public function errors($errors, $code = 500)
    {
        return response()
            ->json([
                "code" => 500,
                "errors" => $errors
            ]);
    }

}
