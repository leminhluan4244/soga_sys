<?php
/**
 * Created by PhpStorm.
 * User: luan.le
 * Date: 19/04/2019
 * Time: 11:51 PM
 */

namespace App\Helpers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RequestHelper
{
    public static $name = "RequestHelper";

    public static function new()
    {
        return new static();
    }

    // Default config of request
    public static $header = [
        "content-type" => "application/json",
        "Accept" => "application/json"
    ];

    // Client object
    public static $client = null;

    // The template on body will send | Lưu trữ danh sách các trường muốn gửi đi
    public static $send = [];

    // The data request | Dữ liệu gửi đi (chưa lọc)
    public static $request = [];


    // The data will send on body | Dữ liệu gửi đi (chưa lọc)
    public static $body = [];

    // The data will get after request to server | Dữ liệu sẽ lấy ra sau khi thực hiện xong request
    public static $result = [];

    // The data will get after request to server | Dữ liệu sẽ lấy ra sau khi thực hiện xong request
    public static $route = "";

    // Request type (GET, POST, PUT, DELETE), default is GET | Kiểu request sẽ gửi (GET, POST, PUT, DELETE) mặc định là GET
    public static $type = "GET";

    /**
     * Config or change header $client
     * @param  array $header
     * @return void
     */
    public static function header($header)
    {
        self::$header["content-type"] = $header["content-type"] ? $header["content-type"] : "";
        self::$header["Accept"] = $header["Accept"] ? $header["Accept"] : "";
    }

    /**
     * Config or change base_uri $client | Cài đặt cấu hình client
     * @param  string $base_uri
     * @return void
     */
    public static function client($base_uri = "http://localhost/public/api/")
    {
        // Create client
        self::$client = new Client([
            "base_uri" => $base_uri,
        ]);
        return self::new();
    }

    /**
     *  Set send values | Cài đặt giá trị sẽ gửi đi
     * @param  array $keys
     * @return object
     */
    public static function body(...$keys)
    {
        self::$body = [];
        if (sizeof($keys) > 0)
            foreach ($keys as $key) {
                self::$body[$key] = null;
            }
        return self::new();
    }

    /**
     *  Set values to body | Lưu giá trị vào body đã cài đặt sẵn
     * @param  array $keys
     * @return object
     */
    private static function set_value_body()
    {
        foreach (self::$body as $key) {
            self::$body[$key] = self::$request[$key];
        }
    }

    /**
     * Config or change $request
     * @param  Request $request
     * @return object
     */
    public static function request(Request $request)
    {
        self::$request = $request;
        return self::new();
    }

    /**
     * Get result | Lấy giá trị từ response nhận được
     * @param  string $value
     * @return object
     */
    public static function get($value = "")
    {

        self::set_value_body();
        $status = self::send();
        if (!$status) { //Return response
            return (object)[
                "code" => 400,
                "errors" => self::$result
            ];
        }
        if ($value == "" || !$status) { //Return response
            return self::$result;
        }
        if (strtolower($value) == "body") {
            return self::$result->getBody();
        } elseif (strtolower($value) == "header") {
            return self::$result->getHeader();
        }
        return (object)[
            "code" => 400,
            "errors" => "You get must be body or header or null value (get all)"
        ];
    }

    /**
     * Set route | Cài đặt giá trị route sẽ đính kèm với path của client để gửi request
     * @param  string $value
     * @return object
     */
    public static function route($value = "", $type = "GET")
    {

        self::$type = $type;
        self::$route = $value;
        return self::new();
    }

    /**
     * Request to server and return response (Type JSON)| Gửi và nhận kiểu JSON từ 1 server
     * @param  string $route
     * @param  string $method
     * @return bool
     */
    public static function send()
    {
        try {
            self::$result = self::$client->request(
                self::$type,
                self::$route,
                [
                    "headers" => self::$header,
                    "body" => json_encode(self::$body)
                ]
            );
            // Get result on body
            return true;
        } catch (Exception $ex) {
            self::$result = $ex;
            return false;
        }
    }

}
