<?php
namespace App\Helpers;

class ConstantHelper {
    // Validate
    const VALID_PASS = 1;
    const VALID_FAIL = -1;
    // Response
    const SUCCESS = 200;
    const VALIDATE = 400;
    const ERROR = 500;
    const EXCEPTION = 404;
}