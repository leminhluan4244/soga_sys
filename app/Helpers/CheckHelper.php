<?php
/**
 * Created by PhpStorm.
 * User: luan.le
 * Date: 20/04/2019
 * Time: 8:04 AM
 */

namespace App\Helpers;

class CheckHelper
{

    public static $name = "CheckHelper";

    public static function isNon($param){
        if($param == null ||
           $param == '' ||
           empty($param)
        ){
            return true;
        }
        return false;
    }

    public static function isVariableName($param){
        if(!is_string($param)){
            return false;
        }
        // $first_char = substr($param, 0, 1);

        // if(in_array($first_char, [""]))
        // return false;
    }
}
