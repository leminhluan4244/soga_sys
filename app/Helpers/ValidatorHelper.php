<?php
/**
 * Created by PhpStorm.
 * User: luan.le
 * Date: 20/04/2019
 * Time: 8:04 AM
 */

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidatorHelper
{

    public static $name = "ValidatorHelper";

    public static function new()
    {
        return new static();
    }

    // Default ruler is empty array / Mặc định mảng quy tắc là rỗng
    public static $ruler = [];

    // Default validator is empty array / Mặc định mảng chứa lỗi validator là rỗng
    public static $validator = [];

    // Default request is null / Mặc định mảng chứa lỗi validator là rỗng
    public static $request = [];

    /**
     * Reset rule validator / Khởi động lại rule validator
     * @param  void
     * @return void
     */
    public static function reset_rule()
    {
        self::$ruler = [];
        return self::new();
    }

    /**
     * Config rule validator / Dùng hàm này để thiết lập một rule
     * @param  array
     * @return void
     */
    public static function rule($value = [])
    {
        self::$ruler = $value;
        return self::new();
    }

    /**
     * Get rule validator / Dùng hàm này để lấy ra rule đã thiết lập
     * @param  void
     * @return array
     */
    public static function get_rule()
    {
        return self::$ruler;
    }

    /**
     * Config $request / Dùng hàm này để thiết request đầu vào
     * @param  array
     * @return void
     */
    public static function request(Request $request)
    {
        self::$request = $request;
        return self::new();
    }

    /**
     * Check validate / Kiểm tra tính hợp lệ dữ liệu
     * @param  Request $request
     * @param  array $validation_rule
     * @return any
     */
    public static function validator()
    {
        // Check the validity of the request
        self::$validator = Validator::make(self::$request->all(), self::$ruler);
        //Return result
        return self::$validator->fails() ? self::$validator->errors() : true;
    }
}
