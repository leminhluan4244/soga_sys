<?php
/**
 * Created by PhpStorm.
 * User: luan.le
 * Date: 20/04/2019
 * Time: 8:04 AM
 */

namespace App\Helpers;

class MakeHelper
{

    public static $name = "MakeHelper";

    public static function isNon($param){
        if($param == null ||
           $param == '' ||
           empty($param)
        ){
            return true;
        }
        return false;
    }
}
