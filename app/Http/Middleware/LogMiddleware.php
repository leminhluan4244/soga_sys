<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Common\LogController as Logger;
use Closure;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param = [
            "url"    => $request->path(),
            "method" => $request->method(),
            "ip"     => $request->ip()
        ];
        Logger::begin($param);

        return $next($request);
    }
}
