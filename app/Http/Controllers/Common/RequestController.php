<?php

namespace App\Http\Controllers;

// Additional use class
use App\Http\Controllers\Common\CommonController;
use Exception;
use App\Http\Controllers\Common\LogController as Logger;
use App\Http\Controllers\Common\ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//Default use class
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="Example API",
     *   version="1.0",
     *   @OA\Contact(
     *     email="support@example.com",
     *     name="Support Team"
     *   )
     * )
     */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // status contants
    const SUCCESS = "SUCCESS";
    const VALIDATE = "VALIDATE";

    // Validation rules
    public $rule = [];

    // Request
    public $request = null;

    // Model class
    public $model = null;

    //Response key
    public $key = "";

    //Response data
    public $data = [];

    // Status of data empty on respone
    public $only_code = true;

    // Status api (success, validate, exception")
    public $status = "SUCCESS";

    // Log object
    public $log = null;

    /** Reset key to default
     * @return void
     */
    public function log_write()
    {
        $this->middleware('log');
    }


    /** Reset key to default
     * @return object
     */
    public function reset()
    {
        $this->rule = [];
        $this->request = null;
        $this->log = null;
        $this->model = null;
        $this->key = "data";
        $this->data = [];
        $this->only_code = true;
        $this->status = self::SUCCESS;
    }

    /** Config this
     * @param  array $option
     * @return object
     */
    public function config($option)
    {
        isset($option["rule"])      ? $this->rule      = $option["rule"]                  : "";
        isset($option["model"])     ? $this->model     = $option["model"]                 : "";
        isset($option["key"])       ? $this->key       = $option["key"]                   : "";
        isset($option["data"])      ? $this->data      = $option["data"]                  : "";
        isset($option["only_code"]) ? $this->only_code = $option["only_code"]             : "";
        isset($option["status"])    ? $this->status    = $option["status"]                : "";
        if (isset($option["request"])) {
            $this->request = $option["request"];
            $this->log     = [
                "url"    => $option["request"]->path(),
                "method" => $option["request"]->method(),
                "ip"     => $option["request"]->ip()
            ];
        }
    }

    /** Check validate
     * @return void
     */
    public function exam()
    {
        if (!empty($this->rule)) {
            // Check the validity of the request
            $validator = Validator::make($this->request->all(), $this->rule);
            // Return Code 400 if request not valid
            if ($validator->fails()) {
                $this->status = self::VALIDATE;
                $this->data = $validator->errors();
            }
        }
    }

    /** Call funciton by string name on model
     * @param  string $function_name
     * @return void
     */
    public function call($function_name)
    {
        // try {
            \DB::beginTransaction();
            $this->exam();
            if ($this->status == self::VALIDATE) {
                \DB::rollBack();

                return ResponseController::errors($this->data, $this->log);
            }
            $request = (array)$this->request->all();
            $this->data = $this->model->$function_name($request);
            \DB::commit();

            return $this->data; // return data on response
        // } catch (Exception $ex) {
        //     \DB::rollBack();

        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Get data from and handle common operations for most controllers
     * @return object
     */
    public function index_full()
    {
        // try {
            $this->exam();
            if ($this->status == self::VALIDATE) {
                return ResponseController::errors($this->data, $this->log);
            }
            // Get all if json request is null
            if (empty($this->request)) {
                $this->data = $this->model->full_data(); //get all row data
            } else {
                $request = (array)$this->request->all();
                $this->data = $this->model->get_data($request); //get all row data
            }

            return $this->response(); // return data on response
        // } catch (Exception $ex) {
        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Insert data from and handle common operations for most controllers
     * @return object
     */
    public function insert_full()
    {
        // try {
            \DB::beginTransaction();
            $param = $this->request;
            // Use APIController
            $this->exam();
            if ($this->status == self::VALIDATE) {
                \DB::rollBack();

                return ResponseController::errors($this->data, $this->log);
            } else {
                $request = (array)$this->request->all();
                $this->model->insert_data($request);
            }
            \DB::commit();

            return $this->response(); // return response don't have data
        // } catch (Exception $ex) {
        //     \DB::rollBack();

        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Update data from and handle common operations for most controllers
     * @return object
     */
    public function update_full()
    {
        // try {
            \DB::beginTransaction();
            $this->exam();
            if ($this->status == self::VALIDATE) {
                \DB::rollBack();

                return ResponseController::errors($this->data, $this->log);
            } else {
                $request = (array)$this->request->all();
                $this->model->update_data($request);
            }
            \DB::commit();

            return $this->response(); // return response don't have data
        // } catch (Exception $ex) {
        //     \DB::rollBack();

        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Update data from and handle common operations for most controllers
     * @return object
     */
    public function delete_full($type = "logic")
    {
        // try {
            $this->exam($this->request);
            if ($this->status == self::VALIDATE) {
                return ResponseController::errors($this->data, $this->log);
            }
            $request = (array)$this->request->all();
            //If null request => then delete all row
            if (empty($this->request)) {
                $this->only_code = true;
                $this->empty_default(); //delete all row data
            } elseif ($type == "logic") {
                $this->model->delete_logic($request);
            } else {
                $this->model->delete_physical($request);
            }

            return $this->response(); // return data on response and rename key;
        // } catch (Exception $ex) {
        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Run return response after handling request
     * @return object
     */
    public function response()
    {
        if ($this->status == self::VALIDATE) {
            return ResponseController::errors($this->data, $this->log);
        }
        if ($this->only_code) { // retrun data and code
            return ResponseController::response();
        }

        // Return only code
        return ResponseController::response($this->key, $this->data);
    }

    public function __destruct()
    {
        Logger::end($this->log);
    }
}
