<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    /** Get data from and handle common operations for most controllers
     * @return object
     */
    public function index()
    {
        // try {
            $this->exam();
            if ($this->status == self::VALIDATE) {
                return ResponseController::errors($this->data, $this->log);
            }
            // Get all if json request is null
            if (empty($this->request)) {
                $this->data = $this->model->full_data(); //get all row data
            } else {
                $request = (array)$this->request->all();
                $this->data = $this->model->get_data($request); //get all row data
            }

            return $this->response(); // return data on response
        // } catch (Exception $ex) {
        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Insert data from and handle common operations for most controllers
     * @return object
     */
    public function insert()
    {
        // try {
            \DB::beginTransaction();
            $param = $this->request;
            // Use APIController
            $this->exam();
            if ($this->status == self::VALIDATE) {
                \DB::rollBack();

                return ResponseController::errors($this->data, $this->log);
            } else {
                $request = (array)$this->request->all();
                $this->model->insert_data($request);
            }
            \DB::commit();

            return $this->response(); // return response don't have data
        // } catch (Exception $ex) {
        //     \DB::rollBack();

        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Update data from and handle common operations for most controllers
     * @return object
     */
    public function update()
    {
         try {
            \DB::beginTransaction();
            $this->exam();
            if ($this->status == self::VALIDATE) {
                \DB::rollBack();

                return ResponseController::errors($this->data, $this->log);
            } else {
                $request = (array)$this->request->all();
                $this->model->update_data($request);
            }
            \DB::commit();

            return $this->response(); // return response don't have data
         } catch (Exception $ex) {
             \DB::rollBack();

             return ResponseController::exception($ex, $this->log);
         }
    }

    /** Update data from and handle common operations for most controllers
     * @return object
     */
    public function delete($type = "logic")
    {
        // try {
            $this->exam($this->request);
            if ($this->status == self::VALIDATE) {
                return ResponseController::errors($this->data, $this->log);
            }
            $request = (array)$this->request->all();
            //If null request => then delete all row
            if (empty($this->request)) {
                $this->only_code = true;
                $this->empty_default(); //delete all row data
            } elseif ($type == "logic") {
                $this->model->delete_logic($request);
            } else {
                $this->model->delete_physical($request);
            }

            return $this->response(); // return data on response and rename key;
        // } catch (Exception $ex) {
        //     return ResponseController::exception($ex, $this->log);
        // }
    }

    /** Run return response after handling request
     * @return object
     */
    public function response()
    {
        if ($this->status == self::VALIDATE) {
            return ResponseController::errors($this->data, $this->log);
        }
        if ($this->only_code) { // retrun data and code
            return ResponseController::response();
        }

        // Return only code
        return ResponseController::response($this->key, $this->data);
    }
}
