<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\LogController as Logger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;

class ResponseController
{
    const SUCCESS = "200";
    const ERROR = "400";
    const EXCEPTION = "500";
    /**
     * Check and return response
     * @param  string $key
     * @param  array $data
     * @param  array $log
     * @return object
     */
    public static function response($key = "", $data = [], $log = null)
    {
        if (!empty($key)) { // return data and code
            return response()
            ->json([
                "code" => self::SUCCESS,
                $key => $data
            ], 200);
        } else { // only return code
            return response()
            ->json([
                "code" => self::SUCCESS,
            ], 200);
        }
    }

    /**
     * Return errors response
     * @param  array $errors
     * @param  object $errors
     * @param  array $log
     * @return object
     */
    public static function errors($errors = ["One of the param values is invalid"], $log = null)
    {
        if ($log != null) {
            Logger::error($errors, $log);
        }
        return response()
                ->json([
                    "code" => self::ERROR,
                    "errors" => $errors
                ], 400);
    }

    /**
     * Return exception response
     * @param  array $exception
     * @param  array $log
     * @return object
     */
    public static function exception($exception = [], $log = null)
    {
        $log != null ? Logger::danger($exception, $log) : "";
        return response()
                ->json([
                    "code" => self::EXCEPTION,
                    "exception" => ["Server not found"]
                ], 500);
    }
}
