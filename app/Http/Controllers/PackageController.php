<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Soga\Generator\DemoController;

class PackageController extends Controller
{
    public function index(){
        $demo = new DemoController();
        return $demo->index();
    }
}
