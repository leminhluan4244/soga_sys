<?php

namespace App\Library\Controllers;

use App\Http\Controllers\Controller;

use Exception;
use DB;
use App\Library\Controllers\SogaValidator;
use App\Library\Controllers\SogaRequest;
use App\Library\Response\SogaResponse;

class SogaController extends Controller
{
    protected $validator = [];
    protected $request = null;
    protected $response = null;
    protected $model = null;
    protected $err = [];


    public function __construct()
    {
        parent::__construct();

        $this->request = new SogaRequest();
        $this->validator = new SogaValidator();
        $this->response = new SogaResponse();
    }

    public function model($model = null)
    {
        if ($model === null) { // null is get model
            return $this->model;
        } elseif (is_string($model)) {
            $this->model = new $model();
        } elseif (is_object($model)) {
            $this->model = $model ;
        }
    }

    public function validate($options=null)
    {
        if ($options === null) { // null is get validator
            return $this->validator;
        }
        if (isset($options['rule'])) {
            $this->validator->rule($options['rule']);
        }
        if (isset($options['message'])) {
            $this->validator->message($options['message']);
        }
    }

    public function config($options){
        $is_err = false;
        if(!isset($options['request']) || CheckHelper::isNon($options['request'])){
            $this->err.push('Please setup request not empty');
            $is_err = true;
        }
        if(!is_object($options['request'])){
            $this->err.push('Please setup request is object');
            $is_err = true;
        }
        if(!isset($options['model']) || CheckHelper::isNon($options['model'])){
            $this->err.push('Please setup model not empty');
            $is_err = true;
        }
        if(!is_object($options['model']) && !is_string($options['model'])){
            $this->err.push('Please setup model is object or string name');
            $is_err = true;
        }
        if(isset($options['rule']) && !is_array($options['rule'])){
            $this->err.push('Please setup rule is array');
            $is_err = true;
        }
        if(isset($options['message']) && !is_array($options['message'])){
            $this->err.push('Please setup message is array');
            $is_err = true;
        }
        if($is_err == false) {
            // Request setup
            $this->request->request($options['request']);
            if (isset($options['filter'])) {
                $this->request->filter($options['filter']);
            }
            // Validate setup
            $this->validate($options);
            // Setup model
            $this->model = $this->model($this->options['model']);
        }
        return $this->err;
    }

    public function call($function_name)
    {
        try {
            // Check config
            if(!empty($this->err)){
                return $this->response::error($this->err);
            }
            // Validate request
            $param = $this->request->param();
            DB::beginTransaction();
            $this->validator->validate($param);
            if ($this->validator->fails()) {
                DB::rollBack();
                return $this->response::errors($this->valid_msg);
            } else {
                $data = $this->model->$function_name($this->request);
                DB::commit();
                return $this->response::success($data);
            }
        } catch (Exception $ex) { // if have exception , rollback and return code 500
            DB::rollBack();
            $exception = [];
            if(config("app")["debug"] == true){
                $exception = $ex;
            }
            return $this->response->exception($exception);
        }
    }
}
