<?php

namespace App\Library\Response;

class SogaResponseVue
{
    const SUCCESS = 200;
    const ERROR = 400;
    const EXCEPTION = 500;
}
