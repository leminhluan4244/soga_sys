<?php

namespace App\Library\Response;

class SogaResponse
{
    function success($param, $message = null){
        return [
            'code' => self::SUCCESS,
            'data' => $param,
            'message' => $message,
            'error' => [],
            'exception' => []
        ];
    }

    function validate($message, $param = null){
        return [
            'code' => self::VALIDATE,
            'data' => $param,
            'message' => $message,
            'error' => [],
            'exception' => []
        ];
    }

    function error($error, $message = null){
        return [
            'code' => self::ERROR,
            'data' => [],
            'message' => $message,
            'error' => $error,
            'exception' => []
        ];
    }

    function exception(){
        return [
            'code' => self::EXCEPTION,
            'data' => [],
            'message' => [],
            'error' => [],
            'exception' => ['Server not found reuqest']
        ];
    }
}
