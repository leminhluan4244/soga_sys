<?php

namespace App\Library\Response;

use Exception;

class SogaResponseWeb
{
    /**
     * Check and return response.
     *
     * @param string $key
     * @param array  $data
     *
     * @return object
     */
    public static function response($key = '', $data = [])
    {
        if (!empty($key)) { // return data and code
            return [
                'code' => self::SUCCESS,
                $key => $data,
            ];
        } else { // only return code
            return self::SUCCESS;
        }
    }

    /**
     * Return errors response.
     *
     * @param array $errors
     *
     * @return object
     */
    public static function errors($errors)
    {
        return [
            'code' => self::ERROR,
            'errors' => $errors,
        ];
    }

    /**
     * Return exception response.
     *
     * @param array|Exception $exception
     *
     * @return object
     */
    public static function exception()
    {
        return  [
            'code' => self::EXCEPTION,
            'exception' => ['Server error, please show log detail'],
        ];
    }
}
