<?php

namespace App\Library\Validator;

use Illuminate\Support\Facades\Validator;

class SogaValidator
{
    // VALIDATE
    private $rule = [];
    private $validate = []; // Result of validate action a request
    private $message = []; // Custom message when validate not success
    private $result = []; // Result of messageGet, message and validate change to format and appended this
    private $status = []; // Result of messageGet, message and validate change to format and appended this

    public function rule($rule = null){
        if($rule === null){
            return $this->rule;
        }
        else{
            $this->rule = $rule;
            return $this->rule;
        }
    }

    public function message($message = null){
        if($message === null){
            return $this->message;
        }
        else{
            $this->message = $message;
            return $this->message;
        }
    }
    // return or make validate
    public function validate($param){
        if($param === null){
            return $this->validate;
        }
        else{
            $this->validate = Validator::make($param, $this->rule, $this->message);
            $this->result();
            return $this->validate;
        }
    }

    public function fails(){
       return $this->validate->fails();
    }

    public function status(){
        return $this->status;
    }

    public function result(){
        if ($this->validate->fails()) {
            $this->status = ConstantHelper::VALID_FAIL;
            $this->result =  $this->validate->errors();
        }
        else{
            $this->status = ConstantHelper::VALID_PASS;
            $this->result =  $this->validate->errors();
        }
    }
}