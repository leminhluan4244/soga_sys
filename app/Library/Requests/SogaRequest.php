<?php

namespace App\Library\Requests;
use Illuminate\Http\Request;

class SogaRequest
{
    public $request;
    public $filters = [];


    public function request(Request $request){
        $this->request = $request;
    }

    public function filter($filters){
        $this->filters = $filters;
    }

    public function param(){
        if($this->filters != null)
        return $this->request->only($this->filters);
    }
}
