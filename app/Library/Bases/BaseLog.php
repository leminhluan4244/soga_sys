<?php

namespace App\Http\Controllers\Base;
use Carbon\Carbon;
use Log;


class BaseLog
{
    public static function info($message, $param)
    {
        Log::info("{$param["method"]} {$param["url"]} {$param["ip"]} >> {$message}\n");
    }

    public static function error($message, $param)
    {
        Log::error("{$param["method"]} {$param["url"]} {$param["ip"]} >> {$message}\n ");
    }

    public static function warning($message, $param)
    {
        Log::warning("{$param["method"]} {$param["url"]} {$param["ip"]} >> {$message}\n");
    }

    public static function danger($message, $param)
    {
        Log::error("{$param["method"]} {$param["url"]} {$param["ip"]} >> {$message}\n");
    }

    public static function begin($param)
    {
        self::info("---------- BEGIN ----------", $param);
    }

    public static function end($param)
    {
        self::info("---------- END ----------", $param);
    }
}
