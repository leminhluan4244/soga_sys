<?php

namespace App\Library\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SogaModel extends Model
{
    const SOFT_DELETE = 1; // CONST check delete row on table
    const ACTIVE = 0; // CONST row is active on table
    const DISABLE_DELETE = -1; // CONST check prohibit delete row on table
    const STATUS_OFF = 0; // STATUS is off
    const STATUS_ON = 1; // STATUS is on
    // List fill want get on request
    protected $fillable_list = [];
    // Conditions where on query
    protected $update_conditions = [];
    protected $delete_conditions = [];

    public function base_search(Request $request)
    {
        $data = self::query();
        $filter_param = $request->only($this->fillable_list);
        if (!empty($filter_param)) {
            foreach ($filter_param as $key => $value) {
                $data = $data->where($key, $value);
            }
        }
        return $data;
    }

    public function base_first(Request $request)
    {
        $data = $this->base_search($request);
        // Nhớ thêm order by để lấy được trường tạo gần đây nhất
        return $data->first();
    }

    public function base_index(Request $request)
    {
        $data = $this->base_search($request);
        return $data->get();
    }

    public function base_insert(Request $request)
    {
        $filter_param = $request->only($this->fillable_list);
        return self::insert($filter_param);
    }

    public function base_update(Request $request)
    {
        $filter_param = $request->only($this->fillable_list);
        // Bỏ đi id trong filter_param
        return self::where($this->update_conditions)
                    ->update($filter_param);
    }

    public function base_delete(Request $request)
    {
        $data = self::query();
        $filter_param = $request->only($this->delete_conditions);
        if (!empty($filter_param)) {
            foreach ($filter_param as $key => $value) {
                $data = $data->where($key, $value);
            }
            return self::where($filter_param)->delete();
        }
    }
}
